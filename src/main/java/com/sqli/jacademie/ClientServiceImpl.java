package com.sqli.jacademie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by edenayrolles on 08/01/2016.
 */
@Service("ClientService")
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientDao clientDao;

    @Override
    public List<Client> findAllClients() {
        return clientDao.findAllClients();
    }

    @Override
    public Client findClientById(long id) {
        return clientDao.findClientById(id);
    }

    @Override
    public List<Client> findClientByName(String name) {
        return clientDao.findClientByName(name);
    }

}
