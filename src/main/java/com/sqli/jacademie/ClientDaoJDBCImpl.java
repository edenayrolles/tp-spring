package com.sqli.jacademie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by edenayrolles on 08/01/2016.
 */
@Repository
public class ClientDaoJDBCImpl implements ClientDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Client> findAllClients() {
        return jdbcTemplate.query("Select * from client limit 10;",
                new RowMapper<Client>() {
                    @Override
                    public Client mapRow(ResultSet resultSet, int i) throws SQLException {
                        Client client = new Client();
                        client.setId(resultSet.getInt(1));
                        client.setNom(resultSet.getString(5));
                        client.setPrenom(resultSet.getString(6));
                        return client;
                    }
                });

        //return jdbcTemplate.query("Select * from client limit 10;", new ClientJDBCMapper());
    }

    @Override
    public Client findClientById(long id) {

        return jdbcTemplate
                .queryForObject("Select * from client where clt_id = ? limit 1;",
                        new Object[]{id},
                        new ClientJDBCMapper());
    }

    @Override
    public List<Client> findClientByName(String name) {
        return jdbcTemplate
                .query("Select * from client where clt_nom = ? limit 10;",
                        new Object[]{name},
                        new ClientJDBCMapper());
    }
}
