package com.sqli.jacademie;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by edenayrolles on 08/01/2016.
 */
public class Start {

    public static void main(String[] args) {
        ApplicationContext applicationContext = loadContextXMLAutoWiring();

        ClientService clientService = applicationContext.getBean("ClientService", ClientService.class);

        System.out.println(clientService.findAllClients());
        System.out.println(clientService.findClientById(1));
        System.out.println(clientService.findClientByName("WAFI"));




    }

    private static ApplicationContext loadContextXML() {
        return new ClassPathXmlApplicationContext("applicationContext.xml");
    }

    private static ApplicationContext loadContextXMLAutoWiring() {
        return new ClassPathXmlApplicationContext("applicationContext-AutoWiring.xml");
    }


}
