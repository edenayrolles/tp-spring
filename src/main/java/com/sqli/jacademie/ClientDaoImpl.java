package com.sqli.jacademie;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edenayrolles on 08/01/2016.
 */
//@Repository
public class ClientDaoImpl implements ClientDao {


    @Override
    public List<Client> findAllClients() {

        List<Client> clientList = new ArrayList<Client>();

        Client clientAAjouter = new Client();
        clientAAjouter.setPrenom("Eric");
        clientAAjouter.setNom("Denayrolles");

        clientList.add(clientAAjouter);

        return clientList;
    }

    @Override
    public Client findClientById(long id) {
        return null;
    }

    @Override
    public List<Client> findClientByName(String name) {
        return null;
    }

}
