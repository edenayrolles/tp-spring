package com.sqli.jacademie;

import java.util.List;

/**
 * Created by edenayrolles on 08/01/2016.
 */
public interface ClientService {

    public List<Client> findAllClients();
    public Client findClientById(long id);
    public List<Client> findClientByName(String name);

}
