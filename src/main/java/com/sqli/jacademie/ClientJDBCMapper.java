package com.sqli.jacademie;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by edenayrolles on 08/01/2016.
 */
public class ClientJDBCMapper implements RowMapper<Client> {
    @Override
    public Client mapRow(ResultSet resultSet, int i) throws SQLException {
        Client client = new Client();
        client.setId(resultSet.getInt(1));
        client.setNom(resultSet.getString(5));
        client.setPrenom(resultSet.getString(6));
        return client;
    }
}
